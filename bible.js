function scripture_loaded(data, bAppend)
{
    var html = data["content"];
    var el = document.getElementById(data["tip"]);
    if (bAppend)
        el.innerHTML += html;
    else
        el.innerHTML = html;
}

function scripture(reference, destId, bAppend)
{
    if (reference == null) {
        log(reference);
    }
	var url = "http://www.esvapi.org/crossref/ref.php?reference=" +
		encodeURIComponent(reference) + "&tip=" + destId;

	jsonp(url, scripture_loaded, bAppend);
}

var getJSONP = 
{
	counter: 0
}

function jsonp(url, callback, bAppend)
{
	var cbnum = "cb" + getJSONP.counter++;
	var cbname = "getJSONP." + cbnum;
	if (url.indexOf("?") === -1)
		url += "?callback=" + cbname;
	else
		url += "&callback=" + cbname;
	var script = document.createElement("script");
	getJSONP[cbnum] = function(response)
	{
		try
		{
		    callback(response, bAppend);
		}
		finally
		{	
			delete getJSONP[cbnum];
			script.parentNode.removeChild(script);
		}
	}
	script.src = url;
	document.body.appendChild(script);
}
