var SeasonsAndFeasts = {

    getAllInfo: function (date) {
        var year = date.getFullYear();
        var season = SeasonsAndFeasts.getSeason(date);
        var seasonStartDate = DateUtils.todayOrVeryNextSunday(season.StartDate);
        var diffDays = DateUtils.daysSince(date, seasonStartDate);
        if (diffDays < 0)
            diffDays = 0;
        var weeksIntoSeason = Math.floor(diffDays / 7);

        if (season.Name == "Ordinary Time") {
            // Adjust weeksIntoSeason so that week=29 always matches the Sunday closest to November 23.
            var sundayNearestNov23 = DateUtils.nearestSunday(new Date(year, 10, 23));
            var weeksUntilThatSunday = Math.floor(DateUtils.daysSince(sundayNearestNov23, date) / 7);
            var weekInOrdinaryTime = 29 - weeksUntilThatSunday - 2;
            weeksIntoSeason = weekInOrdinaryTime;
        }

        var info = {
            Service: DateUtils.morningOrEvening(date),
            Date: date,
            Year: date.getFullYear(),
            Feast: SeasonsAndFeasts.getFeast(date),
            Season: season.Name,
            SeasonDuration: season.Length,
            Week: weeksIntoSeason + 1,
            Weekday: DateUtils.weekday(date),
            Canticles: null,
            Readings: null,
        };

        info.Canticles = Canticles.getCanticles(info);
        info.Readings = DailyLectionary.getReadings(info);

        return info;
    },

    getSeason: function (date) {
        var year = date.getFullYear();
        var seasons = SeasonsAndFeasts.getAllSeasons(year);
        var season = null;
        for (var i = 0; i < seasons.length; ++i)
        {
            if (seasons[i].StartDate <= date)
                season = seasons[i];
            else
                break;
        }
        return season;
    },

    getFeast: function (date) {
        var feasts = SeasonsAndFeasts.getAllFeasts(date.getFullYear());
        for (var i = 0; i < feasts.length; ++i)
        {
            var f = feasts[i];
            if (DateUtils.sameDate(date, f.Date)) {
                if (f.Service != null) {
                    if (DateUtils.morningOrEvening(date) == f.Service)
                        return f;
                }
                else
                    return f;
            }
        }
        return null;
    },

    getAllSeasons: function (year) {
        var feasts = SeasonsAndFeastUtils.getCrucialDates(year);
        var lastChristmas = new Date(year - 1, 11, 25);
        var startOrdinary = DateUtils.offsetDate(feasts.Trinity, 7);
        var result =
        [
            { Name: "Christmas", StartDate: lastChristmas, Length: DateUtils.daysSince(feasts.Epiphany, lastChristmas) },
            { Name: "Epiphany", StartDate: feasts.Epiphany, Length: DateUtils.daysSince(feasts.AshWednesday, feasts.Epiphany) },
            { Name: "Lent", StartDate: feasts.AshWednesday, Length: DateUtils.daysSince(feasts.PalmSunday, feasts.AshWednesday) },
            { Name: "Holy Week", StartDate: feasts.PalmSunday, Length: DateUtils.daysSince(feasts.EasterSunday, feasts.PalmSunday) },
            { Name: "Easter", StartDate: feasts.EasterSunday, Length: DateUtils.daysSince(feasts.Pentecost, feasts.EasterSunday) },
            { Name: "Pentecost", StartDate: feasts.Pentecost, Length: DateUtils.daysSince(startOrdinary, feasts.PalmSunday) },
            { Name: "Ordinary Time", StartDate: startOrdinary, Length: DateUtils.daysSince(feasts.Advent1, startOrdinary) },
            { Name: "Advent", StartDate: feasts.Advent1, Length: DateUtils.daysSince(feasts.Christmas, feasts.Advent1) },
            { Name: "Christmas", StartDate: feasts.Christmas, Length: DateUtils.daysSince(new Date(year+1, 0, 6), feasts.Christmas) },
        ];
        return result;
    },

    storedFeasts: null,

    getAllFeasts: function (year) {
        if (SeasonsAndFeasts.storedFeasts != null && SeasonsAndFeasts.storedFeasts.Year == year)
            return SeasonsAndFeasts.storedFeasts.Feasts;

        // Ranking:
        // 1 = PRINCIPAL FEASTS
        // 2 = The Feasts of the Holy Name, the Presentation, and Transfiguration
        // 3 = Ash Wednesday and Good Friday
        // 4 = Feasts of our Lord
        // 5 = Other Major Feasts
        // 6 = everything else

        var feasts = SeasonsAndFeastUtils.getCrucialDates(year);
        var result = [
            { Name: "Ash Wednesday", Date: feasts.AshWednesday, Rank: 3 },
            { Name: "Good Friday", Date: feasts.GoodFriday, Rank: 3 },
            { Name: "Easter", Date: feasts.EasterSunday, Rank: 1 },
            { ShortName: "Ascension", Name: "Ascension Day", Date: feasts.Ascension, Rank: 1 },
            { Name: "Pentecost", Date: feasts.Pentecost, Rank: 1 },
            { Name: "Trinity", Date: feasts.Trinity, Rank: 1 },
            { ShortName: "Thanksgiving (USA)", Name: "Thanksgiving Day (United States)", Date: feasts.Thanksgiving, Rank: 5 },
            { Name: "Epiphany", Date: feasts.Epiphany, Rank: 1 },
            { ShortName: "The Holy Name", Name: "The Holy Name of Our Lord Jesus Christ", Date: new Date(year, 0, 1), Rank: 2 },
            { ShortName: "Confession of St. Peter", Name: "The Confession of Saint Peter the Apostle", Date: new Date(year, 0, 18) },
            { ShortName: "Conversion of St. Paul", Name: "The Conversion of Saint Paul the Apostle", Date: new Date(year, 0, 25) },
            { Name: "Eve of the Presentation", Date: new Date(year, 1, 23), Service: "Evening" },
            { ShortName: "The Presentation", Name: "The Presentation of Our Lord Jesus Christ in the Temple", Date: new Date(year, 1, 2), Rank: 2 },
            { ShortName: "St. Matthias", Name: "Saint Matthias the Apostle", Date: new Date(year, 1, 24) },
            { ShortName: "St. Joseph", Name: "Saint Joseph", Date: new Date(year, 2, 19) },
            { Name: "Eve of the Annunciation", Date: new Date(year, 2, 24), Service: "Evening" },
            { ShortName: "The Annunciation", Name: "The Annunciation of Our Lord Jesus Christ to the Blessed Virgin Mary", Date: new Date(year, 2, 25), Rank: 4 },
            { ShortName: "St. Mark", Name: "Saint Mark the Evangelist", Date: new Date(year, 3, 25) },
            { ShortName: "St. Philip & St. James", Name: "Saint Philip and Saint James, Apostles", Date: new Date(year, 4, 1) },
            { Name: "Eve of the Visitation", Date: new Date(year, 4, 30), Service: "Evening"},
            { ShortName: "The Visitation", Name: "The Visitation of the Blessed Virgin Mary with Elizabeth", Date: new Date(year, 4, 31), Rank: 4 },
            { ShortName: "St. Barnabas", Name: "Saint Barnabas the Apostle", Date: new Date(year, 5, 11) },
            { Name: "Eve of St. John the Baptist", Date: new Date(year, 5, 23), Service: "Evening" },
            { ShortName: "Nativity of St. John the Baptist", Name: "The Nativity of Saint John the Baptist", Date: new Date(year, 5, 24), Rank: 4 },
            { ShortName: "St. Peter & St. Paul", Name: "Saint Peter and Saint Paul, Apostles", Date: new Date(year, 5, 29) },
            { ShortName: "St. Mary Magdalene", Name: "Saint Mary Magdalene", Date: new Date(year, 6, 22) },
            { ShortName: "St. James", Name: "Saint James the Apostle", Date: new Date(year, 6, 25) },
            { Name: "Eve of the Transfiguration", Date: new Date(year, 7, 6), Service: "Evening"},
            { ShortName: "The Transfiguration", Name: "The Transfiguration of Our Lord Jesus Christ", Date: new Date(year, 7, 6), Rank: 2 },
            { ShortName: "St. Mary", Name: "Saint Mary the Virgin, Mother of Our Lord Jesus Christ", Date: new Date(year, 7, 15) },
            { ShortName: "St. Bartholomew", Name: "Saint Bartholomew the Apostle", Date: new Date(year, 7, 24) },
            { Name: "Eve of Holy Cross", Date: new Date(year, 8, 13), Service: "Evening" },
            { Name: "Holy Cross Day", Date: new Date(year, 8, 14), Rank: 4 },
            { ShortName: "St. Matthew", Name: "Saint Matthew, Apostle and Evangelist", Date: new Date(year, 8, 21) },
            { ShortName: "St. Michael", Name: "Saint Michael and All Angels", Date: new Date(year, 8, 29) },
            { ShortName: "St. Luke", Name: "Saint Luke the Evangelist", Date: new Date(year, 9, 18) },
            { ShortName: "St. James of Jerusalem", Name: "Saint James of Jerusalem, Brother of our Lord Jesus Christ, and Martyr", Date: new Date(year, 9, 23) },
            { ShortName: "St. Simon & St. Jude", Name: "Saint Simon and Saint Jude, Apostles", Date: new Date(year, 9, 28) },
            { Name: "Eve of All Saints", Date: new Date(year, 9, 30), Service: "Evening" },
            { Name: "All Saints", Date: new Date(year, 10, 1), Rank: 1 },
            { ShortName: "St. Andrew", Name: "Saint Andrew the Apostle", Date: new Date(year, 10, 30) },
            { ShortName: "St. Thomas", Name: "Saint Thomas the Apostle", Date: new Date(year, 11, 21) },
            { Name: "Christmas Eve", Date: new Date(year, 11, 24), Service: "Evening" },
            { ShortName: "Christmas", Name: "The Nativity of Jesus Christ", Date: new Date(year, 11, 25), Rank: 1 },
            { ShortName: "St. Stephen", Name: "Saint Stephen, Deacon and Martyr", Date: new Date(year, 11, 26) },
            { ShortName: "St. John", Name: "Saint John, Apostle and Martyr", Date: new Date(year, 11, 27) },
            { Name: "The Holy Innocents", Date: new Date(year, 11, 28) }
        ];

        result.sort(function (a, b) { return a.Date - b.Date; });

        for (var i = 0; i < result.length; ++i)
        {
            if (result[i].ShortName == null)
                result[i].ShortName = result[i].Name;
            if (result[i].Rank == null)
                result[i].Rank = 6;
            if (result[i].Service != null)
                result[i].Date = DateUtils.changeTime(result[i].Date, result[i].Service);
        }

        SeasonsAndFeasts.storedFeasts = { Feasts: result, Year: year };

        return result;
    },

};

var SeasonsAndFeastUtils = {
    easterSunday: function (year) {
        switch (year) {
            case 2010: return new Date(year, 3, 4);
            case 2011: return new Date(year, 3, 24);
            case 2012: return new Date(year, 3, 8);
            case 2013: return new Date(year, 2, 31);
            case 2014: return new Date(year, 3, 20);
            case 2015: return new Date(year, 3, 5);
            case 2016: return new Date(year, 2, 27);
            case 2017: return new Date(year, 3, 16);
            case 2018: return new Date(year, 3, 1);
            case 2019: return new Date(year, 3, 21);
            case 2020: return new Date(year, 3, 12);
            case 2021: return new Date(year, 3, 4);
            case 2022: return new Date(year, 3, 17);
            case 2023: return new Date(year, 3, 9);
            case 2024: return new Date(year, 2, 31);
            default: return null;
        }
    },

    getCrucialDates: function (year) {
        var stAndrew = new Date(year, 10, 30); // 30 Nov
        var epiphany = new Date(year, 0, 6);
        var easter = SeasonsAndFeastUtils.easterSunday(year);
        var pentecost = DateUtils.offsetDate(easter, 49);
        var nov1 = new Date(year, 10, 1);
        // S M T W  R  F  S
        // 0 1 2 3  4  5  6
        // 7 8 9 10 11 12 13
        // If Nov 1 is a Thursday, 
        var thanksgiving = null;
        if (nov1.getDay() < 4) // S,M,T,W
            thanksgiving = DateUtils.offsetDate(nov1, 4 - nov1.getDay() + 21);
        else
            thanksgiving = DateUtils.offsetDate(nov1, 11 - nov1.getDay() + 21);
        var result = {
            Advent1: DateUtils.nearestSunday(stAndrew),
            Christmas: new Date(year, 11, 25),
            Epiphany: epiphany,
            EasterSunday: easter,
            AshWednesday: DateUtils.offsetDate(easter, -46),
            PalmSunday: DateUtils.offsetDate(easter, -7),
            GoodFriday: DateUtils.offsetDate(easter, -2),
            Pentecost: pentecost, // 50 days after Easter, inclusive
            Ascension: DateUtils.offsetDate(pentecost, -3), // Thursday before Pentecost
            Trinity: DateUtils.offsetDate(pentecost, 7),
            Thanksgiving: thanksgiving
        };
        return result;
    },

} // end SeasonsAndFeasts


var DateUtils = {
    daysSince: function (to, from) {
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        var diffDays = Math.round((to.getTime() - from.getTime()) / (oneDay));
        return diffDays;
    },

    weekday: function (date) {
        day = date.getDay(); // day of the week 
        switch (day) {
            case 0: return "Sunday";
            case 1: return "Monday";
            case 2: return "Tuesday";
            case 3: return "Wednesday";
            case 4: return "Thursday";
            case 5: return "Friday";
            case 6: return "Saturday";
        }
        return "Invalid Day of the Week";
    },

    daynum: function (dayname) {
        var names = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        for (var i = 0; i < names.length; ++i)
            if (names[i] == dayname)
                return i;
        return 0;
    },

    sameDate: function (d1, d2) {
        return d1.getDate() == d2.getDate() && d1.getMonth() == d2.getMonth();
        //return DateUtils.day_of_year(d1) == DateUtils.day_of_year(d2);
    },

    day_of_year: function (date) {
        jan1 = new Date(date.getFullYear(), 0, 1);
        return Math.ceil((date - jan1) / 86400000);
    },

    offsetDate: function (date, offsetDays) {
        result = new Date(date);
        result.setDate(date.getDate() + offsetDays);
        return result;
    },

    nearestSunday: function (date) {
        day = date.getDay(); // day of the week 
        offset = (day <= 3) ? -day : 7 - day;
        return DateUtils.offsetDate(date, offset);
    },

    todayOrVeryNextSunday: function (date) {
        day = date.getDay(); // day of the week 
        var offset = 0;
        if (day > 0)
            offset = 7 - day;
        var result = DateUtils.offsetDate(date, offset);
        return result;

    },

    sundayAfter: function (date) {
        day = date.getDay(); // day of the week 
        offset = 7 - day;
        return DateUtils.offsetDate(date, offset);
    },

    morningOrEvening: function (date) {
        if (date.getHours() >= 12)
            return "Evening";
        return "Morning";
    },

    changeTime: function (date, service) {
        var d = new Date(date);
        if (service == "Morning")
            d.setHours(8);
        else
            d.setHours(20);
        return d;
    },

    getMonthName: function (month) {
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        return monthNames[month];
    },
};

