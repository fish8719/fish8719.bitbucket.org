
var Canticles = {
    getCanticles: function (info) {
        if (info.Service == "Morning")
            return Canticles.getMorningCanticles(info);
        else
            return Canticles.getEveningCanticles(info);
    },

    getMorningCanticles: function (info) {
        var result = {
            Invitatory: "Venite",
            Lesson1: "Benedictus Dominus",
            Lesson2: "Te Deum"
        };
        switch (info.Weekday)
        {
            case "Sunday":
                if (info.Season == "Advent")
                    result.Lesson1 = "Ecce Deus";
                else if (info.Season == "Lent")
                    result.Lesson1 = "Kyrie Pantokrator";
                else if (info.Season == "Easter")
                    result.Lesson1 = "Cantemus Domino";
                else
                    result.Lesson1 = "Benedictus Dominus";
                if (info.Season == "Lent" || info.Season == "Advent")
                    result.Lesson2 = "Benedictus Dominus";
                else
                    result.Lesson2 = "Te Deum";
                break;
            case "Monday":
                result.Lesson1 = "Ecce Deus";
                result.Lesson2 = "Magna et mirabilia";
                break;
            case "Tuesday":
                result.Lesson1 = "Benedictus es Domine";
                result.Lesson2 = "Dignus es";
                break;
            case "Wednesday":
                if (info.Season == "Lent")
                    result.Lesson1 = "Kyrie Pantokrator";
                else
                    result.Lesson1 = "Surge illuminare";
                result.Lesson2 = "Benedictus Dominus";
                break;
            case "Thursday":
                if (info.Season == "Lent" || info.Season == "Advent")
                    result.Lesson1 = "Magna et mirabilia";
                else
                    result.Lesson1 = "Cantemus Domino";
                result.Lesson2 = "Gloria in excelsis";
                break;
            case "Friday":
                if (info.Season == "Lent")
                    result.Lesson1 = "Kyrie Pantokrator";
                else
                    result.Lesson1 = "Magna et mirabilia";
                result.Lesson2 = "Dignus es";
                break;
            case "Saturday":
                result.Lesson1 = "Benedicite";
                result.Lesson2 = "Magna et mirabilia";
                break;
        }
        if (info.Feast != null && info.Feast.Rank <= 5)
        {
            result.Lesson1 = "Benedictus Dominus";
            result.Lesson2 = "Te Deum";
        }
        return result;
    },


    getEveningCanticles: function (info) {
        var result = {
            Invitatory: "Phos Hilaron",
            Lesson1: "Magnificat",
            Lesson2: "Nunc Dimittis",
        };
        switch (info.Weekday) {
            case "Sunday":
                break;
            case "Monday":
                break;
            case "Tuesday":
                break;
            case "Wednesday":
                break;
            case "Thursday":
                break;
            case "Friday":
                break;
            case "Saturday":
                break;
        }
        return result;
    },

};