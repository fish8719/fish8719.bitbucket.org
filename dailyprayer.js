function log(msg) {
    document.getElementById("debug").innerHTML += msg + "<br>";
}

function clearlog() {
    document.getElementById("debug").innerHTML = "";
}

var DailyPrayer = {
    date: new Date(), // current date and time
    info: null,
    readings: null,
    loadedReadings: false,

    setInfo: function (info) {
        DailyPrayer.info = info;
        DailyPrayer.loadedReadings = false;
    },

    onPageLoad: function () {
        DailyPrayer.setInfo( SeasonsAndFeasts.getAllInfo(DailyPrayer.date) );
        DailyPrayer.setControls();

        PageUtils.hideElementById("canticleLibrary");
        PageUtils.loadDiv("canticles.html", "canticleLibrary", DailyPrayer.configureService);

        PageUtils.loadDiv("collects.html", "collectOfTheDay", DailyPrayer.configureService);
        DailyPrayer.configureService();
    },

    configureService: function () {
        DailyPrayer.configReadings();
        DailyPrayer.configDateHeading();
        DailyPrayer.configByAttribute();
        DailyPrayer.configCanticles();
    },

    configReadings: function () {
        if (DailyPrayer.info.Readings == null)
        {
            document.getElementById("Psalms").innerHTML = "";
            document.getElementById("Lesson1").innerHTML = "";
            document.getElementById("Lesson2").innerHTML = "";
            return;
        }

        if (DailyPrayer.loadedReadings == true)
            return;
        DailyPrayer.loadedReadings = true;

        var r = DailyPrayer.info.Readings;

        var ps = r.Psalms;
        document.getElementById("Psalms").innerHTML = "";
        for (var i = 0; i < ps.length; ++i)
            scripture("Psalms " + ps[i].toString(), "Psalms", true);

        var l1 = null;
        var l2 = null;
        if (DailyPrayer.info.Service == "Morning")
        {
            l1 = r.OldTestament;
            l2 = r.Epistle;
        }
        else
        {
            l2 = r.Gospel;
            if (DailyPrayer.info.Date.getDay() % 2 == 0)
                l1 = r.OldTestament;
            else
                l1 = r.Epistle;
        }

        scripture(l1, "Lesson1", false);
        scripture(l2, "Lesson2", false);
    },

    configCanticles: function () {
        c1 = DailyPrayer.findCanticle(DailyPrayer.info.Canticles.Invitatory);
        if (c1 != null)
            document.getElementById("InvitatoryCanticle").innerHTML = c1;

        c2 = DailyPrayer.findCanticle(DailyPrayer.info.Canticles.Lesson1);
        if (c2 != null)
            document.getElementById("CanticleLesson1").innerHTML = c2;

        if (DailyPrayer.info.Canticles.Lesson2 != null)
        {
            c3 = DailyPrayer.findCanticle(DailyPrayer.info.Canticles.Lesson2);
            if (c3 != null)
                document.getElementById("CanticleLesson2").innerHTML = c3;
        }
    },

    findCanticle: function(name) {
        canticles = document.getElementsByClassName("canticle");
        for (var i = 0; i < canticles.length; ++i)
        {
            if (canticles[i].getAttribute("data-canticle") == name)
                return canticles[i].innerHTML;
        }
        return "";
    },

    gotoWeekday: function (dayname) {
        var date = DailyPrayer.date;
        var daynum = DateUtils.daynum(dayname);
        var currentDay = date.getDay();
        var newDate = DateUtils.offsetDate(date, daynum - currentDay);
        DailyPrayer.date = newDate;
        DailyPrayer.setInfo( SeasonsAndFeasts.getAllInfo(newDate) );
        DailyPrayer.setControls();
        DailyPrayer.configureService();
    },

    gotoFeast: function (feastname) {
        clearlog();
        var date = DailyPrayer.date;
        var year = date.getFullYear();
        var feasts = SeasonsAndFeasts.getAllFeasts(year);
        for (var i = 0; i < feasts.length; ++i) {
            if (feasts[i].ShortName == feastname) {
                date = feasts[i].Date;
                if (feasts[i].Service == null) {
                    var currentHour = date.getHours();
                    date.setHours(currentHour);
                }
                break;
            }
        }
        DailyPrayer.date = date;
        DailyPrayer.setInfo( SeasonsAndFeasts.getAllInfo(date) );
        DailyPrayer.setControls();
        DailyPrayer.configureService();
    },

    gotoService: function (service) {
        if (service == "Morning")
            DailyPrayer.date.setHours(3); // sometime in the morning
        else
            DailyPrayer.date.setHours(20); // sometime in the evening
        DailyPrayer.setInfo( SeasonsAndFeasts.getAllInfo(DailyPrayer.date) );
        DailyPrayer.setControls();
        DailyPrayer.configureService();
    },

    gotoYesterday: function () {
        clearlog();
        var date = DailyPrayer.date;
        date = DateUtils.offsetDate(date, -1);
        DailyPrayer.date = date;
        DailyPrayer.setInfo( SeasonsAndFeasts.getAllInfo(date) );
        DailyPrayer.setControls();
        DailyPrayer.configureService();
    },

    gotoToday: function () {
        clearlog();
        var date = new Date();
        DailyPrayer.date = date;
        DailyPrayer.setInfo( SeasonsAndFeasts.getAllInfo(date) );
        DailyPrayer.setControls();
        DailyPrayer.configureService();
    },

    gotoTomorrow: function () {
        clearlog();
        var date = DailyPrayer.date;
        date = DateUtils.offsetDate(date, 1);
        DailyPrayer.date = date;
        DailyPrayer.setInfo( SeasonsAndFeasts.getAllInfo(date) );
        DailyPrayer.setControls();
        DailyPrayer.configureService();
    },

    setControls: function () {
        var ctrls = {
            Service: document.getElementById("cfgService"),
            Weekday: document.getElementById("cfgWeekday"),
            Feast: document.getElementById("cfgFeast")
        };

        ctrls.Service.value = DailyPrayer.info.Service;

        PageUtils.selectItemByValue(ctrls.Weekday, DailyPrayer.info.Weekday);

        ctrls.Feast.innerHTML = "<option></option>";
        var feasts = SeasonsAndFeasts.getAllFeasts(DailyPrayer.info.Year);
        for (var i = 0; i < feasts.length; ++i)
            ctrls.Feast.innerHTML += "<option>" + feasts[i].ShortName + "</option>";
        if (DailyPrayer.info.Feast != null)
            PageUtils.selectItemByValue(ctrls.Feast, DailyPrayer.info.Feast.ShortName);
    },

    configDateHeading: function () {
        var info = DailyPrayer.info;
        html = "";
        if (DailyPrayer.info.Feast != null)
            html = "<h2>" + DailyPrayer.info.Feast.Name + "</h2>";
        html += "<h3>" + info.Weekday + ", ";
        if (info.SeasonDuration > 7) {
            var ending = "th";
            if (info.Week == 1)
                ending = "st";
            else if (info.Week <= 3)
                ending = "rd";
            html += "the " + info.Week + ending + " Week in ";
        }
        html += DailyPrayer.info.Season;
        html += " (" + DateUtils.getMonthName(DailyPrayer.date.getMonth()) + " " + DailyPrayer.date.getDate() + ")";
        html += "</h3>";
        document.getElementById("DateHeading").innerHTML = html;
    },

    configByAttribute: function () {
        var divs = document.getElementsByTagName("div");
        var pars = document.getElementsByTagName("p");
        for (var i = 0, max = divs.length; i < max; ++i) {
            DailyPrayer.configElement(divs[i]);
        }
        for (var i = 0, max = pars.length; i < max; ++i) {
            DailyPrayer.configElement(pars[i]);
        }
    },

    configElement: function (elem) {
        var info = DailyPrayer.info;
        var hide = false;
        var show = false;

        var id = elem.id;

        var feasts = elem.getAttribute("data-feast");
        var seasons = elem.getAttribute("data-season");
        var seasonsExcluded = elem.getAttribute("data-excludeseason");
        var days = elem.getAttribute("data-day");
        var weeks = elem.getAttribute("data-week");
        var service = elem.getAttribute("data-service");

        if (feasts != null || seasons != null || seasonsExcluded != null ||
            days != null || weeks != null || service != null)
            show = true;

        if (service != null && !PageUtils.listContains(service, info.Service))
            hide = true;
        if (seasons != null && !PageUtils.listContains(seasons, info.Season))
            hide = true;
        if (feasts != null && (info.Feast == null || !PageUtils.listContains(feasts, info.Feast.ShortName)))
            hide = true;
        if (days != null && !PageUtils.listContains(days, info.Weekday))
            hide = true;
        if (weeks != null && !PageUtils.listContains(weeks, info.Week.toString()))
            hide = true;
        if (seasonsExcluded != null && PageUtils.listContains(seasonsExcluded, info.Season))
            hide = true;

        if (hide == true)
            PageUtils.hideElement(elem);
        else if (show == true)
            PageUtils.showElement(elem);
        },

};

var PageUtils = {
    selectItemByValue: function (elmnt, value) {
        if (elmnt == null)
            return;
        for (var i = 0; i < elmnt.options.length; i++) {
            if (elmnt.options[i].value == value)
                elmnt.selectedIndex = i;
        }
    },

    listContains: function (list, item) {
        if (list == null || item == null)
            return false;
        var items = list.split(',');
        for (var i = 0; i < items.length; ++i) {
            if (items[i] == item)
                return true;
        }
        return false;
    },

    hideElementById: function (id) {
        document.getElementById(id).style.display = 'none';
    },

    hideElement: function (el) {
        el.style.display = 'none';
    },

    showElement: function (el) {
        el.style.display = 'block';
    },

    loadDiv: function (url, divname, callWhenDone) {
        var div = document.getElementById(divname);
        var http = new XMLHttpRequest();
        http.onreadystatechange = function (e) {
            if (http.readyState == 4 && http.status == 200) {
                div.innerHTML = http.responseText;
                if (callWhenDone != null)
                    callWhenDone();
            }
        }
        http.open("GET", url, true);
        http.setRequestHeader('Content-type', 'text/html');
        http.send();
    }
};

//function handleResponse() {
//    if (http.readyState == 4) {
//        var response = http.responseText;
//        document.getElementById('setADivWithAnIDWhereYouWantIt').innerHTML = response;
//    }
//}

//function configLectionary(lect) {
//    if (lect.OldTestament != null)
//        scripture(lect.OldTestament, "OldTestament");
//    else
//        PageUtils.hideElementById("OldTestament");
//    if (lect.Epistle != null)
//        scripture(lect.Epistle, "Epistle");
//    else
//        PageUtils.hideElementById("Epistle");
//    if (lect.Gospel != null)
//        scripture(lect.Gospel, "Gospel");
//    else
//        PageUtils.hideElementById("Gospel");
//}

